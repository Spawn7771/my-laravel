FROM php:7.4-fpm-alpine

# Set working directory
WORKDIR '/var/www'

COPY ./composer.lock ./composer.json /var/www/

RUN set -ex \
    && apk update && apk upgrade\
# Installations into virtual env so they can be deleted afterwards
# (.phpize-deps is standardized by docker-php-ext-install)
    && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS \
    && apk add --no-cache --virtual .build-deps \
    libstdc++ \
    make \ 
# Install system dependencies
#RUN apk update && apk add --no-cache \
    curl \
    libbz2 \
    libzip-dev \
    zlib-dev \
    bzip2-dev \
    libxslt-dev \
    libmcrypt-dev \
    libxml2-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    yaml-dev \
    libaio-dev \
    oniguruma-dev \
    php7-bz2 \
    zip \
    unzip

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

#Install redis extension for supporting into Laravel
RUN pecl install redis
#RUN echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini
RUN docker-php-ext-enable redis

# Clear cache
#rm -rf /var/cache/apk/*

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Copy existing application directory contents
COPY ./ /var/www


RUN apk del .phpize-deps .build-deps \
*-dev

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
